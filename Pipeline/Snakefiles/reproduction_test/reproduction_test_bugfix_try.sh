#echo Kallisto_index_download
#wget ftp://ftp.ensembl.org/pub/release-90/fasta/mus_musculus/cdna/Mus_musculus.GRCm38.cdna.all.fa.gz -O /media/Enbio/Tobias/R_Git_Sonstiges/Pipeline/Snakefiles/reproduction_test/index/Mus_musculus.GRCm38.cdna.all.fa.gz
#echo Kallisto_index_build:
#kallisto index -i /media/Enbio/Tobias/R_Git_Sonstiges/Pipeline/Snakefiles/reproduction_test/index/mus_musculus_ref_GRCm38.idx /media/Enbio/Tobias/R_Git_Sonstiges/Pipeline/Snakefiles/reproduction_test/index/Mus_musculus.GRCm38.cdna.all.fa.gz
#echo Kallisto_quant:
#kallisto quant -i /media/Enbio/Tobias/R_Git_Sonstiges/Pipeline/Snakefiles/reproduction_test/index/mus_musculus_ref_GRCm38.idx -b 35 -t 8 -o /media/Enbio/Tobias/R_Git_Sonstiges/Pipeline/Snakefiles/reproduction_test/output/Res_P1 /media/Enbio/Tobias/Data/Trimmomatic/P1_out_R1_paired.fastq.gz /media/Enbio/Tobias/Data/Trimmomatic/P1_out_R2_paired.fastq.gz
#echo fastqc:
#fastqc /media/transfer/RNASeq/cleanData/P1.clean_1.fastq.gz -o /media/Enbio/Tobias/R_Git_Sonstiges/Pipeline/Snakefiles/reproduction_test/fastq_after_trim/ -f fastq
#fastqc /media/transfer/RNASeq/cleanData/P1.clean_2.fastq.gz -o /media/Enbio/Tobias/R_Git_Sonstiges/Pipeline/Snakefiles/reproduction_test/fastq_after_trim/ -f fastq
echo trimmomatic:
trimmomatic PE -threads 4 -phred33 
-trimlog /media/Enbio/Tobias/R_Git_Sonstiges/Pipeline/Snakefiles/reproduction_test/trimmomatic/_clean.log 
/media/Enbio/Tobias/Data/zcat/P1_R1_zcat.fastq 
/media/Enbio/Tobias/Data/zcat/P1_R2_zcat.fastq 
/media/Enbio/Tobias/R_Git_Sonstiges/Pipeline/Snakefiles/reproduction_test/trimmomatic/P1_out_R1_paired.fastq.gz 
/media/Enbio/Tobias/R_Git_Sonstiges/Pipeline/Snakefiles/reproduction_test/trimmomatic/P1_out_R1_unpaired.fastq.gz 
/media/Enbio/Tobias/R_Git_Sonstiges/Pipeline/Snakefiles/reproduction_test/trimmomatic/P1_out_R2_paired.fastq.gz 
/media/Enbio/Tobias/R_Git_Sonstiges/Pipeline/Snakefiles/reproduction_test/trimmomatic/P1_out_R2_unpaired.fastq.gz 
ILLUMINACLIP:adapters.fasta:2:30:10 AVGQUAL:20

echo fastqc_after_trim
fastqc /media/Enbio/Tobias/R_Git_Sonstiges/Pipeline/Snakefiles/reproduction_test/trimmomatic/P1_out_R1_paired.fastq.gz -o /media/Enbio/Tobias/R_Git_Sonstiges/Pipeline/Snakefiles/reproduction_test/fastq_after_trim_own/ -f fastq
fastqc /media/Enbio/Tobias/R_Git_Sonstiges/Pipeline/Snakefiles/reproduction_test/trimmomatic/P1_out_R2_paired.fastq.gz -o /media/Enbio/Tobias/R_Git_Sonstiges/Pipeline/Snakefiles/reproduction_test/fastq_after_trim_own/ -f fastq

echo Kallisto_quant_own_data
kallisto quant -i /media/Enbio/Tobias/Data/Kallisto_cal/mus_musculus_ref_GRCm39_V104.idx -b 35 -t 8 -o /media/Enbio/Tobias/R_Git_Sonstiges/Pipeline/Snakefiles/reproduction_test/output/Res_P1_AVGQUAL_new_ref /media/Enbio/Tobias/R_Git_Sonstiges/Pipeline/Snakefiles/reproduction_test/trimmomatic/P1_out_R1_paired.fastq.gz /media/Enbio/Tobias/R_Git_Sonstiges/Pipeline/Snakefiles/reproduction_test/trimmomatic/P1_out_R2_paired.fastq.gz 


fastqc /media/Enbio/Tobias/R_Git_Sonstiges/Pipeline/Snakefiles/miniSnake/old_packs/90/trimmingD/R2_paired.fastq.gz -o /media/Enbio/Tobias/R_Git_Sonstiges/Pipeline/Snakefiles/miniSnake/old_packs/90/Fastqc_P1_R2/ -f fastq