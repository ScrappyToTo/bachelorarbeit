#Parameter:
cfgLocation = "/media/Enbio/Tobias/R_Git_Sonstiges/Pipeline/Snakefiles/Snakefile_Chip_Pipeline/" # must end with a slash
cfgName = "configRun2.tsv"

## Global
numberOfThreads = 7
availableMemory = 30 #for bamQC
##rule trimFastq
trimmingLength = 60 #-trim_to_len
minQualMean = 20#-min_qual_mean 20
##rule align:
pathToRef = "/media/Enbio/Christoph/PhD-Work_DataUScrips/FremdDaten/refGenome/mm10_Ref_UCSC/mm10.fa" #my ref aber diffrent folder
maxDiff = 4
##rule trimBam
bamQualiValue = 30


import pandas as pd
samples = pd.read_table(cfgLocation+cfgName).set_index("sampleName", drop=False)
#print(cfgLocation+cfgName)
#print(samples)


#WD+<directory of the input file>
#FILE+<input file name>
#---------------------THREADS+10
#RG+@RG\\tID:<tID>\\tLB:<tLB>\\tSM:<tSM>\\tPL:<tPL>
#++++++++++++++++++++++#TRIM+1
#-----------------TRIM_CO+60
#-----------------MEAN_QUAL+20
#------------------DIFF+4
#++++++PRINSEQ+<path to prinseq-lite>/
#++++++++++BWA+<path to bwa>/
#++++++++++PICARD+<path to picard-tools>/
#FASTQX+<path to fastqx>/
#-----------------------REF_GENOME+<reference chromosome index file with fullpath>
#++++++++++++++SAMTOOLS+<path to samtools>/
#++++++++++++++++QUALIMAP+<path>/qualimap
#-------------------BAM_QUALI+30
#+++++++++++++++TMP+/home/nfs_data/lange/chip-seq/temp
#+++++++++++++++FASTQC+<path>/fastqc
#++++++++++++++TMP_DIR+<tmp dir>


rule all:
    input:
        #General Quality Checks
        expand(cfgLocation+"data/quality/raw_fastq/{id}_fastqc.html", id=samples.sampleName),
        expand(cfgLocation+"data/quality/bamQC/{id}_trim/qualimapReport.html", id=samples.sampleName),
        #Main Result
        #expand(cfgLocation+"data/aln_final/{id}_trim_MQ_DR_CR.bam", id=samples.sampleName),
        expand(cfgLocation+"data/aln_final_BW/{id}_trim_MQ_DR_CR.bw", id=samples.sampleName)
        # To shy to delete
       # expand('data/fastq_merged/{id}.fastq', id= samples.sampleName),

rule mergeFasq:
    input:
        lambda wc: samples[samples.sampleName == wc.id].folderPath.iloc[0]
    output:
        cfgLocation+"data/fastqMerged/{id}.fastq"
    shell:
        "zcat '{input}'/*.fastq.gz > '{output}'"

rule sra2fastq:#unused at the moment
    input:
        cfgLocation+"data/sra/{id}.sra"
    output:
        cfgLocation+"data/fastq/{id}.fastq"
    shell:
        "fastq-dump {input} -O "+cfgLocation+"data/fastq/"

rule fastqQC:
    input:
        in1=cfgLocation+"data/fastqMerged/{id}.fastq",
        in2=cfgLocation+"data/fastqTrim/{id}_trim.fastq"
    params:
        t=numberOfThreads
    output:
        out1=cfgLocation+"data/quality/raw_fastq/{id}_fastqc.html",
        out2=cfgLocation+"data/quality/trim_fastq/{id}_trim_fastqc.html"
    shell:
        """
        fastqc {input.in1} -o {cfgLocation}data/quality/raw_fastq -t {params.t} -f fastq
        fastqc {input.in2} -o {cfgLocation}data/quality/trim_fastq -t {params.t} -f fastq
        """

rule trimFastq:
    input:
        cfgLocation+"data/fastqMerged/{id}.fastq"
    params:
        l=trimmingLength,
        q=minQualMean
    output:
        cfgLocation+"data/fastqTrim/{id}_trim.fastq"
    shell:
        "prinseq-lite.pl -fastq {input} -out_good "+cfgLocation+"data/fastqTrim/{wildcards.id}_trim -trim_to_len {params.l} -log "+cfgLocation+"data/trimmig.log -out_bad "+cfgLocation+"data/fastqTrim/{wildcards.id}_BADtrim -min_qual_mean {params.q}"


#Example Header: @RG\tID:S1_12-11-14_H3K27me3\tLB:S1_12-11-14_H3K27me3\tSM:S1_12-11-14_H3K27me3\tPL:ILLUMINA
                #@RG\tID:300                 \tLB:SAH3K4me3PC         \tSM:SAH3K4me3PC         \tPL:ILLUMINA

rule align:
    input:
        cfgLocation+"data/fastqTrim/{id}_trim.fastq"
    params:
        r=pathToRef,
        t=numberOfThreads,
        d=maxDiff,
        tLB=lambda wc: samples[samples.sampleName == wc.id].sampleName.iloc[0],
        tSM=lambda wc: samples[samples.sampleName == wc.id].sampleName.iloc[0],
        tID=lambda wc: samples[samples.sampleName == wc.id].ID.iloc[0],
        tPL=lambda wc: samples[samples.sampleName == wc.id].Platform.iloc[0]
    output:
        alnOut=cfgLocation+"data/aln/{id}_trim.sai",
        samseOut=cfgLocation+"data/aln/{id}_trim.sam",
    shell:
        """
        bwa aln -n {params.d} -t {params.t} -f {output.alnOut} {params.r} {input}
        bwa samse -f {output.samseOut} -r '@RG\\tID:{params.tID}\\tLB:{params.tLB}\\tSM:{params.tSM}\\tPL:{params.tPL}' {params.r} {output.alnOut} {input}
        """

rule sam2bam:
    input:
        cfgLocation+"data/aln/{id}_trim.sam"
    output:
        cfgLocation+"data/aln/{id}_trim.bam"
    shell:
        "picard SortSam SO=coordinate TMP_DIR="+cfgLocation+"ZS/picard INPUT={input} OUTPUT={output} CREATE_INDEX=true VALIDATION_STRINGENCY=LENIENT"

rule bamQC:
    input:
        in1=cfgLocation+"data/aln/{id}_trim.bam",
        in2=cfgLocation+"data/aln/{id}_trim_MQ.bam",
        in3=cfgLocation+"data/aln/{id}_trim_MQ_DR.bam",
        in4=cfgLocation+"data/aln_final/{id}_trim_MQ_DR_CR.bam"
    params:
        m=availableMemory,
        t=numberOfThreads
    output:
        out1=cfgLocation+"data/quality/bamQC/{id}_trim/qualimapReport.html",
        out2=cfgLocation+"data/quality/bamQC/{id}_trim_MQ/qualimapReport.html",
        out3=cfgLocation+"data/quality/bamQC/{id}_trim_MQ_DR/qualimapReport.html",
        out4=cfgLocation+"data/quality/bamQC/{id}_trim_MQ_DR_CR/qualimapReport.html"
    shell:
        """
        qualimap bamqc  --java-mem-size={params.m}G -bam {input.in1} -c -nt {params.t} -gd MOUSE -outformat HTML -outdir {cfgLocation}data/quality/bamQC/{wildcards.id}_trim
        qualimap bamqc  --java-mem-size={params.m}G -bam {input.in2} -c -nt {params.t} -gd MOUSE -outformat HTML -outdir {cfgLocation}data/quality/bamQC/{wildcards.id}_trim_MQ
        qualimap bamqc  --java-mem-size={params.m}G -bam {input.in3} -c -nt {params.t} -gd MOUSE -outformat HTML -outdir {cfgLocation}data/quality/bamQC/{wildcards.id}_trim_MQ_DR
        qualimap bamqc  --java-mem-size={params.m}G -bam {input.in4} -c -nt {params.t} -gd MOUSE -outformat HTML -outdir {cfgLocation}data/quality/bamQC/{wildcards.id}_trim_MQ_DR_CR
        """

rule trimBam:
    ### remove reads with mapping quality below BAM_Quali and unmapped reads (-F 4)
    input:
        cfgLocation+"data/aln/{id}_trim.bam"
    params:
        q=bamQualiValue
    output:
        out1=cfgLocation+"data/aln/{id}_trim_MQ.bam",
        out2=cfgLocation+"data/aln/{id}_trim_MQ.bai"
    shell:
        """
        samtools view -b -F 4 -q {params.q} {input} > {output.out1}
        samtools index {output.out1} {output.out2}
        """
rule dupRemove:
    input:
        cfgLocation+"data/aln/{id}_trim_MQ.bam"
    output:
        cfgLocation+"data/aln/{id}_trim_MQ_DR.bam"
    shell:
        "picard MarkDuplicates INPUT={input} OUTPUT={output} TMP_DIR="+cfgLocation+"ZS/duplicationRemoval METRICS_FILE="+cfgLocation+"ZS/duplicationRemoval/{wildcards.id}.metrics REMOVE_DUPLICATES=TRUE CREATE_INDEX=TRUE VALIDATION_STRINGENCY=LENIENT"

rule chrRemove:
    input:
        cfgLocation+"data/aln/{id}_trim_MQ_DR.bam"
    output:
        tmp=temp(cfgLocation+"ZS/{id}_trim_MQ_DR_CR.bam"),
        out1=cfgLocation+"data/aln_final/{id}_trim_MQ_DR_CR.bam",
        out2=cfgLocation+"data/aln_final/{id}_trim_MQ_DR_CR.bai"
    shell:
        """
        samtools view -b -o {output.tmp} {input} chr1 chr2 chr3 chr4 chr5 chr6 chr7 chr8 chr9 chr10 chr11 chr12 chr13 chr14 chr15 chr16 chr17 chr18 chr19 chrX chrY
        samtools sort -o {output.out1} {output.tmp}
        samtools index {output.out1} {output.out2}
        """

rule creatBW:
    input:
        cfgLocation+"data/aln_final/{id}_trim_MQ_DR_CR.bam"
    output:
        cfgLocation+"data/aln_final_BW/{id}_trim_MQ_DR_CR.bw"
    shell:
        "bamCoverage -b {input} -o {output}"
