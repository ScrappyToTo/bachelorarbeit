R1p="/media/Enbio/Tobias/R_Git_Sonstiges/Pipeline/Snakefiles/miniSnake/trimmingNoChop/R1_paired.fastq.gz"
R2p="/media/Enbio/Tobias/R_Git_Sonstiges/Pipeline/Snakefiles/miniSnake/trimmingNoChop/R2_paired.fastq.gz"

echo "Start downloading"
wget ftp://ftp.ensembl.org/pub/release-89/fasta/mus_musculus/cdna/Mus_musculus.GRCm38.cdna.all.fa.gz -O Mus_musculus.GRCm38_89.cdna.all.fa.gz
wget ftp://ftp.ensembl.org/pub/release-90/fasta/mus_musculus/cdna/Mus_musculus.GRCm38.cdna.all.fa.gz -O Mus_musculus.GRCm38_90.cdna.all.fa.gz
echo "start Indexing"
kallisto index -i Mus_musculus.GRCm38_89.idx Mus_musculus.GRCm38_89.cdna.all.fa.gz
kallisto index -i Mus_musculus.GRCm38_90.idx Mus_musculus.GRCm38_90.cdna.all.fa.gz
echo "start quanting"
kallisto quant -i Mus_musculus.GRCm38_89.idx -b 35 -t 8 -o P1Res89 $R1p $R2p
kallisto quant -i Mus_musculus.GRCm38_90.idx -b 35 -t 8 -o P1Res90 $R1p $R2p
echo "Finished"