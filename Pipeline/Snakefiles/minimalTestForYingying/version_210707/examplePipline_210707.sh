mkdir down
mkdir out

echo Kallisto_index_download:
wget ftp://ftp.ensembl.org/pub/release-89/fasta/mus_musculus/cdna/Mus_musculus.GRCm38.cdna.all.fa.gz -O down/Mus_musculus.GRCm38.cdna.all.fa.gz
# wget http://hgdownload.cse.ucsc.edu/goldenpath/mm10/bigZips/mrna.fa.gz  #oder ggf diese version, dann aber komplett veraltet (zenodo version)
        
echo transcript2gene_downloads:
wget ftp://ftp.ensembl.org/pub/release-89/gtf/mus_musculus/Mus_musculus.GRCm38.89.gtf.gz -O down/Mus_musculus.GRCm38.89.gtf.gz
wget https://github.com/BUStools/getting_started/releases/download/getting_started/t2g.py -O down/t2g.py  #checked
        
echo zcat:
zcat /media/transfer/RNASeq/RNA-Seq-20-8-17/Project_BC17_60b_Wuelling/Sample_P1/P1_*_R1_*.fastq.gz > out/P1_R1_zcat.fastq
zcat /media/transfer/RNASeq/RNA-Seq-20-8-17/Project_BC17_60b_Wuelling/Sample_P1/P1_*_R2_*.fastq.gz > out/P1_R2_zcat.fastq
        
echo Kallisto_index_build:
kallisto index -i out/mus_musculus_ref_GRCm38.idx down/Mus_musculus.GRCm38.cdna.all.fa.gz

echo gunzip_Index:
gunzip -c down/Mus_musculus.GRCm38.89.gtf.gz > out/Mus_musculus.GRCm38.89.gtf

echo trimmomatic:
#trimmomatic PE -threads 4 -phred33 -trimlog out/_clean.log out/P1_R1_zcat.fastq out/P1_R1_zcat.fastq out/P1_out_R1_paired.fastq.gz out/P1_out_R1_unpaired.fastq.gz out/P1_out_R2_paired.fastq.gz out/P1_out_R2_unpaired.fastq.gz ILLUMINACLIP:adapters.fasta:2:30:10 HEADCROP:10 LEADING:20 TRAILING:20 SLIDINGWINDOW:4:20 MINLEN:36
trimmomatic PE -threads 4 -phred33 -trimlog out/_clean.log out/P1_R1_zcat.fastq out/P1_R1_zcat.fastq out/P1_out_R1_paired.fastq.gz out/P1_out_R1_unpaired.fastq.gz out/P1_out_R2_paired.fastq.gz out/P1_out_R2_unpaired.fastq.gz ILLUMINACLIP:adapters.fasta:2:30:10 HEADCROP:10 LEADING:20 TRAILING:20 SLIDINGWINDOW:4:20 MINLEN:50 #minlen to zenodoversion
        
echo transcript2gene_build:
python3 down/t2g.py -v < out/Mus_musculus.GRCm38.89.gtf > out/trancript2gene.txt 

echo Kallisto_qant:
kallisto quant -i out/mus_musculus_ref_GRCm38.idx -b 35 -t 8 -o out/P1Res out/P1_out_R1_paired.fastq.gz out/P1_out_R2_paired.fastq.gz

