# README

This project contains the pipelines and scripts to my bachelor thesis: Identification of new regulatory genes during articular cartilage development.

The Pipeline structure, RNA analysis parameter and ChIP seq analysis is based of the paper: Wuelling, M. et al. Epigenetic Mechanisms Mediating Cell State Transitions in Chondrocytes. J. Bone Miner. Res. 00, 1–18 (2021) [Go to the article](https://asbmr.onlinelibrary.wiley.com/doi/10.1002/jbmr.4263)

The table below shows the pathes to the different datas that were integrated in the bachelor thesis.

|Path|Data|
|------------|------------|
|"Pipeline/Snakefiles/Snakefile"|Pipeline for RNA-seq analysis/IntePareto|
|"Pipeline/Snakefiles/Snakefile_Chip_Pipeline/Snakefile"|Pipeline for ChiP-seq analysis|
|"R-Daten_sauber"|Scripts integrated in RNA-seq/IntePareto Pipeline|
|"R_work_places"|All scripts that were created during the bachelor thesis|


