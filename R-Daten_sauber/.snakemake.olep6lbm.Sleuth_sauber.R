
######## Snakemake header ########
library(methods)
Snakemake <- setClass(
    "Snakemake",
    slots = c(
        input = "list",
        output = "list",
        params = "list",
        wildcards = "list",
        threads = "numeric",
        log = "list",
        resources = "list",
        config = "list",
        rule = "character"
    )
)
snakemake <- Snakemake(
    input = list('/media/Enbio/Tobias/Data/tpms_sleuth/PG.sleuth.results.WT.gene_log2FC.csv', '/media/Enbio/Tobias/Data/tpms_sleuth/HG.sleuth.results.WT.gene_log2FC.csv', '/media/Enbio/Tobias/Data/tpms_sleuth/PHsleuth.results.WT.gene_log2FC.csv', '/media/Enbio/Tobias/Data/Kallisto_cal/results/Kallisto_res', '/media/Enbio/Tobias/Data/Transcript2Gene/trancript2gene.txt'),
    output = list('/media/Enbio/Tobias/R_Git_Sonstiges/Pipeline/Outputs/ZS/tpms_gene_1tpm_PHG.csv', '/media/Enbio/Tobias/R_Git_Sonstiges/Pipeline/Outputs/ZS/PHG.sleuth.obj.RData', '/media/Enbio/Tobias/R_Git_Sonstiges/Pipeline/Outputs/diff_exp/PG_new_sleuth_results_WT_gene_log2FC.csv', '/media/Enbio/Tobias/R_Git_Sonstiges/Pipeline/Outputs/diff_exp/HG_new_sleuth_results_WT_gene_log2FC.csv', '/media/Enbio/Tobias/R_Git_Sonstiges/Pipeline/Outputs/diff_exp/PH_new_sleuth_results_WT_log2FC.csv', '/media/Enbio/Tobias/R_Git_Sonstiges/Pipeline/Outputs/diff_exp/GP_new_sleuth_results_WT_log2FC.csv', '/media/Enbio/Tobias/R_Git_Sonstiges/Pipeline/Outputs/diff_exp/GH_new_sleuth_results_WT_log2FC.csv', '/media/Enbio/Tobias/R_Git_Sonstiges/Pipeline/Outputs/diff_exp/HP_new_sleuth_results_WT_log2FC.csv', '/media/Enbio/Tobias/R_Git_Sonstiges/Pipeline/Outputs/diff_exp/Sleuth_results_merged.csv', '/media/Enbio/Tobias/R_Git_Sonstiges/Pipeline/Outputs/vis/Origins_tpms_plot.png', '/media/Enbio/Tobias/R_Git_Sonstiges/Pipeline/Outputs/vis/new_and_old_tpms_plots.png', '/media/Enbio/Tobias/R_Git_Sonstiges/Pipeline/Outputs/vis/b_value_plots.png', '/media/Enbio/Tobias/R_Git_Sonstiges/Pipeline/Outputs/vis/b_value_rev_plots.png', '/media/Enbio/Tobias/R_Git_Sonstiges/Pipeline/Outputs/ZS/tpms_gene_PHG.csv'),
    params = list(),
    wildcards = list(),
    threads = 1,
    log = list(),
    resources = list(),
    config = list(),
    rule = 'Sleuth'
)
######## Original script #########
## Preparations for the sleuth analysis PHG

# load required packages
#devtools::install_github("pachterlab/sleuth")
#install.packages("BiocManager")
#BiocManager::install("rhdf5")
library("biomaRt")
library("sleuth")
library(ggplot2)
library(gridExtra)

##Snakefile Inputs
Origin_PG_Sleuth <- snakemake@input[[1]]
Origin_HG_Sleuth <- snakemake@input[[2]]
Origin_PH_Sleuth <- snakemake@input[[3]]
Mapping_file <- snakemake@input[[4]]
transcript2gene <- snakemake@input[[5]] 


##Snakefile outputs
tpms_gene_1tpm_PHG <- snakemake@output[[1]]
PHG.sleuth.obj.RData <- snakemake@output[[2]]
PG_new_sleuth_results_WT_gene_log2FC_path <-  snakemake@output[[3]]
HG_new_sleuth_results_WT_gene_log2FC_path <- snakemake@output[[4]]
PH_new_sleuth_results_WT_log2FC_path <- snakemake@output[[5]]
GP_new_sleuth_results_WT_log2FC_path <- snakemake@output[[6]]
GH_new_sleuth_results_WT_log2FC_path <- snakemake@output[[7]]
HP_new_sleuth_results_WT_log2FC_path <-  snakemake@output[[8]]
Origins_tpms_plot.png <- snakemake@output[[9]]
new_and_old_tpms_plots.png <- snakemake@output[[10]]
b_value_plots.png <- snakemake@output[[11]]
b_value_rev_plots.png <- snakemake@output[[12]]
tpms_gene_PHG_path <- snakemake@output[[13]]
 

##Read old differential expression analysis

Origin_PG_Sleuth <- read.csv(Origin_PG_Sleuth)
Origin_HG_Sleuth <- read.csv(Origin_HG_Sleuth)
Origin_PH_Sleuth <- read.csv(Origin_PH_Sleuth)



# create an ausillary table that describes the experimental design and the relationship between the kallisto directories and the samples
mapping.file <- data.frame(sample = c("P1","P2","H3","H4","G5","G6"),
                           condition = c("P","P","H","H","G","G"),
                           stringsAsFactors = FALSE)


# append a new column to describe the path of the kallisto quantifications
mapping.file$path <- paste(Mapping_file, mapping.file$sample, sep = '')

transcript2gene <- read.delim(transcript2gene, header = TRUE, sep = "\t")


## gene-level tpms output after filtering 1 tpm genes

s2c <- mapping.file

## G is the reference level because of the alphabetic order

sleuth.obj <- sleuth_prep(sample_to_covariates = s2c, 
                          target_mapping = transcript2gene, 
                          extra_bootstrap_summary = TRUE,
                          read_bootstrap_tpm = TRUE, 
                          aggregation_column = 'ens_gene',
                          gene_mode = TRUE,
                          num_cores = 4)

sleuth.obj$gene_mode



tpms <- sleuth_to_matrix(sleuth.obj, "obs_norm", "tpm")

tpms <- as.data.frame(tpms)
tpms$ens_gene <- rownames(tpms)
tpms$ext_gene <- sleuth.obj$target_mapping$ext_gene[match(tpms$ens_gene, sleuth.obj$target_mapping$ens_gene)]
rownames(tpms) <- NULL
write.csv(tpms, file = tpms_gene_PHG_path, row.names = FALSE)

ind <- rowSums(tpms[,1:6]>=1) >= 6

tpms <- tpms[ind,]
write.csv(tpms, file = tpms_gene_1tpm_PHG, row.names = FALSE)


## check the filtering 

tpms_gene_1tpm <- read.csv(tpms_gene_1tpm_PHG, header = TRUE, stringsAsFactors = FALSE)
genefilter <- tpms_gene_1tpm$ens_gene
txfilter <- transcript2gene[transcript2gene$ens_gene%in%genefilter,]
sleuth.obj <- sleuth_prep(sample_to_covariates = s2c, 
                          target_mapping = txfilter, 
                          extra_bootstrap_summary = TRUE,
                          # filter_target_id = txfilter,
                          read_bootstrap_tpm = TRUE, 
                          aggregation_column = 'ens_gene',
                          pval_aggregate = TRUE,
                          gene_mode = TRUE,
                          num_cores = 4)
tpms2 <- sleuth_to_matrix(sleuth.obj, "obs_norm", "tpm")
tpms2 <- as.data.frame(tpms2)
plot(tpms$H3, tpms2$H3) ## the same
unique(tpms$H3==tpms2$H3)
save(sleuth.obj, file = PHG.sleuth.obj.RData)

load(PHG.sleuth.obj.RData)
library(ggplot2)
sleuth::plot_pca(obj = sleuth.obj, 
                 color_by = 'condition',
                 units = 'scaled_reads_per_base',
                 text_labels = TRUE)+
  ggtitle("PCA")+
  theme_bw()



## do Differential (gene) expression analysis with sleuth after filtering 1 TPM genes for PG

## Differential (gene) expression analysis with sleuth

#Internally, with sleuth_prep Sleuth will transform elements in the condition field to 0s and 1s in alphabetical order and then WT's beta values will be relative to the 0 condition; that is, positive beta values showing transcripts in which expression is greater in condition 1 than in condition 0.
# 1. Fit the full model
sleuth.obj <- sleuth::sleuth_fit(obj = sleuth.obj, 
                                 formula = ~condition, 
                                 fit_name = 'full')
design_matrix(sleuth.obj)
# 2. fit a reduced model that only includes the intercept
sleuth.obj <- sleuth::sleuth_fit(obj = sleuth.obj, 
                                 formula = ~1, 
                                 fit_name = 'reduced')

models(sleuth.obj)
# 3. Compute the differential expression statistics WT test

# WT is used because it generates the beta statistic, which approximates to the log2 fold change in expression between the 2 condition tested.
sleuth.obj.wt <- sleuth::sleuth_wt(obj = sleuth.obj, 
                                   which_beta = "conditionP", 
                                   which_model = 'full')


# 4. Extract the statistics with WT test
sleuth.results.WT <- sleuth_results(obj = sleuth.obj.wt, 
                                    test = "conditionP", 
                                    test_type = "wt", 
                                    which_model = "full",
                                    show_all = FALSE, 
                                    pval_aggregate = sleuth.obj.wt$pval_aggregate)

PG_new_sleuth_results_WT_gene_log2FC <- merge(tpms, sleuth.results.WT, by.x = c("ens_gene","ext_gene"), by.y = c("target_id","ext_gene"))

write.csv(x = PG_new_sleuth_results_WT_gene_log2FC, file = PG_new_sleuth_results_WT_gene_log2FC_path)
nrow(sleuth.results.WT[sleuth.results.WT$qval<=0.05,])




## do Differential (gene) expression analysis with sleuth after filtering 1 TPM genes reference for HG

# Compute the differential expression statistics WT test
# WT is used because it generates the beta statistic, which approximates to the log2 fold change in expression between the 2 condition tested.
sleuth.obj.wt <- sleuth::sleuth_wt(obj = sleuth.obj, 
                                   which_beta = "conditionH", 
                                   which_model = 'full')


# 4. Extract the statistics with WT test
sleuth.results.WT <- sleuth_results(obj = sleuth.obj.wt, 
                                    test = "conditionH", 
                                    test_type = "wt", 
                                    which_model = "full",
                                    show_all = FALSE, 
                                    pval_aggregate = sleuth.obj.wt$pval_aggregate)

HG_new_sleuth_results_WT_gene_log2FC <- merge(tpms, sleuth.results.WT, by.x = c("ens_gene","ext_gene"), by.y = c("target_id","ext_gene"))

write.csv(x = HG_new_sleuth_results_WT_gene_log2FC, file = HG_new_sleuth_results_WT_gene_log2FC_path)
nrow(sleuth.results.WT[sleuth.results.WT$qval<=0.05,])



## do Differential (gene) expression analysis with sleuth after filtering 1 TPM genes reference for PH

new_s2c <- s2c
new_s2c$condition <- as.factor(new_s2c$condition)
new_s2c$condition <- relevel(new_s2c$condition, ref = "P")


## the new_s2c table now has P as the reference level



so2 <- sleuth_prep(sample_to_covariates = new_s2c, 
                   target_mapping = transcript2gene, 
                   extra_bootstrap_summary = TRUE,
                   read_bootstrap_tpm = TRUE, 
                   aggregation_column = 'ens_gene',
                   gene_mode = TRUE,
                   num_cores = 4)


so2 <- sleuth::sleuth_fit(obj = so2, 
                          formula = ~condition, 
                          fit_name = 'full')
design_matrix(so2)
# 2. fit a reduced model that only includes the intercept
so2 <- sleuth::sleuth_fit(obj = so2, 
                          formula = ~1, 
                          fit_name = 'reduced')

models(so2)
# Compute the differential expression statistics WT test
# WT is used becase it generates the beta statistic, which approximates to the log2 fold change in expression between the 2 condition tested.
so2.wt <- sleuth::sleuth_wt(obj = so2, 
                            which_beta = "conditionH", 
                            which_model = 'full')


# Extract the statistics with WT test
so2.WT <- sleuth_results(obj = so2.wt, 
                         test = "conditionH", 
                         test_type = "wt", 
                         which_model = "full",
                         show_all = FALSE, 
                         pval_aggregate = so2.wt$pval_aggregate)

PH_new_sleuth_results_WT_log2FC <- merge(tpms, so2.WT, by.x = c("ens_gene","ext_gene"), by.y = c("target_id","ext_gene"))

write.csv(x = PH_new_sleuth_results_WT_log2FC, file = PH_new_sleuth_results_WT_log2FC_path)



##repetition of GP differential gene expression data in reverved order for varification


# Compute the differential expression statistics WT test
# WT is used because it generates the beta statistic, which approximates to the log2 fold change in expression between the 2 condition tested.

so2.wt <- sleuth::sleuth_wt(obj = so2, 
                            which_beta = "conditionG", 
                            which_model = 'full')


# Extract the statistics with WT test
so2.WT <- sleuth_results(obj = so2.wt, 
                         test = "conditionG", 
                         test_type = "wt", 
                         which_model = "full",
                         show_all = FALSE, 
                         pval_aggregate = so2.wt$pval_aggregate)

GP_new_sleuth_results_WT_log2FC <- merge(tpms, so2.WT, by.x = c("ens_gene","ext_gene"), by.y = c("target_id","ext_gene"))

write.csv(x = GP_new_sleuth_results_WT_log2FC, file = GP_new_sleuth_results_WT_log2FC_path)





##repetition of GH differential gene expression data in reverved order for varification

new_s2c_H <- s2c
new_s2c_H$condition <- as.factor(new_s2c_H$condition)
new_s2c_H$condition <- relevel(new_s2c_H$condition, ref = "H")


## the new_s2c_H table now has H as the reference level


so2_H <- sleuth_prep(sample_to_covariates = new_s2c_H, 
                     target_mapping = transcript2gene, 
                     extra_bootstrap_summary = TRUE,
                     read_bootstrap_tpm = TRUE, 
                     aggregation_column = 'ens_gene',
                     gene_mode = TRUE,
                     num_cores = 4)


so2_H <- sleuth::sleuth_fit(obj = so2_H, 
                            formula = ~condition, 
                            fit_name = 'full')
design_matrix(so2_H)
# fit a reduced model that only includes the intercept
so2_H <- sleuth::sleuth_fit(obj = so2_H, 
                            formula = ~1, 
                            fit_name = 'reduced')

models(so2_H)
# Compute the differential expression statistics WT test
# WT is used becase it generates the beta statistic, which approximates to the log2 fold change in expression between the 2 condition tested.
so2_H.wt <- sleuth::sleuth_wt(obj = so2_H, 
                              which_beta = "conditionG", 
                              which_model = 'full')


# Extract the statistics with WT test
so2_H.WT <- sleuth_results(obj = so2_H.wt, 
                           test = "conditionG", 
                           test_type = "wt", 
                           which_model = "full",
                           show_all = FALSE, 
                           pval_aggregate = so2_H.wt$pval_aggregate)

GH_new_sleuth_results_WT_log2FC <- merge(tpms, so2_H.WT, by.x = c("ens_gene","ext_gene"), by.y = c("target_id","ext_gene"))

write.csv(x = GH_new_sleuth_results_WT_log2FC, file = GH_new_sleuth_results_WT_log2FC_path)



##repetition of HP differential gene expression data in reverved order for varification

# Compute the differential expression statistics WT test
# WT is used becase it generates the beta statistic, which approximates to the log2 fold change in expression between the 2 condition tested.
so2_H.wt <- sleuth::sleuth_wt(obj = so2_H, 
                              which_beta = "conditionP", 
                              which_model = 'full')


# Extract the statistics with WT test
so2_H.WT <- sleuth_results(obj = so2_H.wt, 
                           test = "conditionP", 
                           test_type = "wt", 
                           which_model = "full",
                           show_all = FALSE, 
                           pval_aggregate = so2_H.wt$pval_aggregate)

HP_new_sleuth_results_WT_log2FC <- merge(tpms, so2_H.WT, by.x = c("ens_gene","ext_gene"), by.y = c("target_id","ext_gene"))

write.csv(x = HP_new_sleuth_results_WT_log2FC, file = HP_new_sleuth_results_WT_log2FC_path)



##Plot for tpms abberation visualisation before script modification

tpms_origin_PG_G5 <- data.frame(Origin_PG_Sleuth$G5, Origin_PG_Sleuth$ext_gene)
names(tpms_origin_PG_G5) <- c("PG_tpms_origin_G5", "ext_gene")
tpms_origin_PG_P1 <- data.frame(Origin_PG_Sleuth$P1, Origin_PG_Sleuth$ext_gene)
names(tpms_origin_PG_P1) <- c("PG_tpms_origin_P1", "ext_gene")

tpms_origin_HG_G5 <- data.frame(Origin_HG_Sleuth$G5, Origin_HG_Sleuth$ext_gene)
names(tpms_origin_HG_G5) <- c("HG_tpms_origin_G5", "ext_gene")
tpms_origin_HG_H3 <- data.frame(Origin_HG_Sleuth$H3, Origin_HG_Sleuth$ext_gene)
names(tpms_origin_HG_H3) <- c("HG_tpms_origin_H3", "ext_gene")

tpms_origin_PH_P1 <- data.frame(Origin_PH_Sleuth$P1, Origin_PH_Sleuth$ext_gene)
names(tpms_origin_PH_P1) <- c("PH_tpms_origin_P1", "ext_gene")
tpms_origin_PH_H3 <- data.frame(Origin_PH_Sleuth$H3, Origin_PH_Sleuth$ext_gene)
names(tpms_origin_PH_H3) <- c("PH_tpms_origin_H3", "ext_gene")

tpms_origins <- merge(tpms_origin_PG_G5,tpms_origin_PG_P1, by = "ext_gene") 
tpms_origins <- merge(tpms_origins, tpms_origin_HG_G5, by= "ext_gene")                  
tpms_origins <- merge(tpms_origins, tpms_origin_HG_H3, by= "ext_gene")                      
tpms_origins <- merge(tpms_origins, tpms_origin_PH_P1, by= "ext_gene")
tpms_origins <- merge(tpms_origins, tpms_origin_PH_H3, by= "ext_gene") 

tpms_origins <- subset(tpms_origins, PG_tpms_origin_G5 < 5000)

Plot_a <- ggplot(tpms_origins, aes(PG_tpms_origin_G5, HG_tpms_origin_G5))+geom_point()
Plot_b <- ggplot(tpms_origins, aes(PG_tpms_origin_P1, PH_tpms_origin_P1))+geom_point()
Plot_c <- ggplot(tpms_origins, aes(HG_tpms_origin_H3, PH_tpms_origin_H3))+geom_point()


Origins_tpms_plot <-grid.arrange(Plot_a, Plot_b, Plot_c, ncol = 2)
ggsave(Origins_tpms_plot, file = Origins_tpms_plot.png, width = 9, height = 9 )




##Plot for tpms abberation visualisation after script modification

tpms_new_PG_G5 <- data.frame(PG_new_sleuth_results_WT_gene_log2FC$G5, PG_new_sleuth_results_WT_gene_log2FC$ext_gene)
names(tpms_new_PG_G5) <- c("PG_tpms_new_G5", "ext_gene")
tpms_new_PG_P1 <- data.frame(PG_new_sleuth_results_WT_gene_log2FC$P1, PG_new_sleuth_results_WT_gene_log2FC$ext_gene)
names(tpms_new_PG_P1) <- c("PG_tpms_new_P1", "ext_gene")

tpms_new_HG_G5 <- data.frame(HG_new_sleuth_results_WT_gene_log2FC$G5, HG_new_sleuth_results_WT_gene_log2FC$ext_gene)
names(tpms_new_HG_G5) <- c("HG_tpms_new_G5", "ext_gene")
tpms_new_HG_H3 <- data.frame(HG_new_sleuth_results_WT_gene_log2FC$H3, HG_new_sleuth_results_WT_gene_log2FC$ext_gene)
names(tpms_new_HG_H3) <- c("HG_tpms_new_H3", "ext_gene")

tpms_new_PH_P1 <- data.frame(PH_new_sleuth_results_WT_log2FC$P1, PH_new_sleuth_results_WT_log2FC$ext_gene)
names(tpms_new_PH_P1) <- c("PH_tpms_new_P1", "ext_gene")
tpms_new_PH_H3 <- data.frame(PH_new_sleuth_results_WT_log2FC$H3, PH_new_sleuth_results_WT_log2FC$ext_gene)
names(tpms_new_PH_H3) <- c("PH_tpms_new_H3", "ext_gene")

tpms_new <- merge(tpms_new_PG_G5,tpms_new_PG_P1, by = "ext_gene") 
tpms_new <- merge(tpms_new, tpms_new_HG_G5, by= "ext_gene")                  
tpms_new <- merge(tpms_new, tpms_new_HG_H3, by= "ext_gene")                      
tpms_new <- merge(tpms_new, tpms_new_PH_P1, by= "ext_gene")
tpms_new <- merge(tpms_new, tpms_new_PH_H3, by= "ext_gene") 

tpms_new <- subset(tpms_new, PG_tpms_new_G5 < 5000)


Plot_d <- ggplot(tpms_new, aes(PG_tpms_new_G5, HG_tpms_new_G5))+geom_point()
Plot_e <- ggplot(tpms_new, aes(PG_tpms_new_P1, PH_tpms_new_P1))+geom_point()
Plot_f <- ggplot(tpms_new, aes(HG_tpms_new_H3, PH_tpms_new_H3))+geom_point()


new_and_old_tpms_plots <-grid.arrange(Plot_a, Plot_b, Plot_c, Plot_d, Plot_e, Plot_f,  ncol = 2)
ggsave(new_and_old_tpms_plots, file = new_and_old_tpms_plots.png, width = 9, height = 9 )




##b-value comparison of new and original data

b_new_PG <- data.frame(PG_new_sleuth_results_WT_gene_log2FC$b, PG_new_sleuth_results_WT_gene_log2FC$ext_gene)
names(b_new_PG) <- c("b_new_PG", "ext_gene")
b_origin_PG <- data.frame(Origin_PG_Sleuth$b, Origin_PG_Sleuth$ext_gene)
names(b_origin_PG) <- c("b_origin_PG", "ext_gene")

b_new_HG <- data.frame(HG_new_sleuth_results_WT_gene_log2FC$b, HG_new_sleuth_results_WT_gene_log2FC$ext_gene)
names(b_new_HG) <- c("b_new_HG", "ext_gene")
b_origin_HG <- data.frame(Origin_HG_Sleuth$b, Origin_HG_Sleuth$ext_gene)
names(b_origin_HG) <- c("b_origin_HG", "ext_gene")

b_new_PH <- data.frame(PH_new_sleuth_results_WT_log2FC$b, PH_new_sleuth_results_WT_log2FC$ext_gene)
names(b_new_PH) <- c("b_new_PH", "ext_gene")
b_origin_PH <- data.frame(Origin_PH_Sleuth$b, Origin_PH_Sleuth$ext_gene)
names(b_origin_PH) <- c("b_origin_PH", "ext_gene")

b_value_comp <- merge(b_new_PG,b_origin_PG, by = "ext_gene") 
b_value_comp <- merge(b_value_comp,b_new_HG, by = "ext_gene") 
b_value_comp <- merge(b_value_comp,b_origin_HG, by = "ext_gene") 
b_value_comp <- merge(b_value_comp,b_new_PH, by = "ext_gene") 
b_value_comp <- merge(b_value_comp,b_origin_PH, by = "ext_gene") 

Plot_g <- ggplot(b_value_comp, aes(b_new_PG, b_origin_PG))+geom_point()
Plot_h <- ggplot(b_value_comp, aes(b_new_HG, b_origin_HG))+geom_point()
Plot_i <- ggplot(b_value_comp, aes(b_new_PH, b_origin_PH))+geom_point()

b_value_plots <-grid.arrange(Plot_g, Plot_h, Plot_i, ncol = 2)
ggsave(b_value_plots, file = b_value_plots.png, width = 9, height = 9 )





##b-value comparison of reversed analysis

b_new_PG <- data.frame(PG_new_sleuth_results_WT_gene_log2FC$b, PG_new_sleuth_results_WT_gene_log2FC$ext_gene)
names(b_new_PG) <- c("b_new_PG", "ext_gene")

b_new_HG <- data.frame(HG_new_sleuth_results_WT_gene_log2FC$b, HG_new_sleuth_results_WT_gene_log2FC$ext_gene)
names(b_new_HG) <- c("b_new_HG", "ext_gene")

b_new_PH <- data.frame(PH_new_sleuth_results_WT_log2FC$b, PH_new_sleuth_results_WT_log2FC$ext_gene)
names(b_new_PH) <- c("b_new_PH", "ext_gene")

b_rv_GP <- data.frame(GP_new_sleuth_results_WT_log2FC$b, GP_new_sleuth_results_WT_log2FC$ext_gene)
names(b_rv_GP) <- c("b_rv_GP", "ext_gene")

b_rv_GH <- data.frame(GH_new_sleuth_results_WT_log2FC$b, GH_new_sleuth_results_WT_log2FC$ext_gene)
names(b_rv_GH) <- c("b_rv_GH", "ext_gene")

b_rv_HP <- data.frame(HP_new_sleuth_results_WT_log2FC$b, HP_new_sleuth_results_WT_log2FC$ext_gene)
names(b_rv_HP) <- c("b_rv_HP", "ext_gene")


b_value_rev_comp <- merge(b_new_PG,b_new_HG, by = "ext_gene") 
b_value_rev_comp <- merge(b_value_rev_comp,b_new_PH, by = "ext_gene") 
b_value_rev_comp <- merge(b_value_rev_comp,b_rv_GP, by = "ext_gene") 
b_value_rev_comp <- merge(b_value_rev_comp,b_rv_GH, by = "ext_gene") 
b_value_rev_comp <- merge(b_value_rev_comp,b_rv_HP, by = "ext_gene") 

Plot_j <- ggplot(b_value_rev_comp, aes(b_new_PG, b_rv_GP))+geom_point()
Plot_k <- ggplot(b_value_rev_comp, aes(b_new_HG, b_rv_GH))+geom_point()
Plot_l <- ggplot(b_value_rev_comp, aes(b_new_PH, b_rv_HP))+geom_point()

b_value_rev_plots <-grid.arrange(Plot_j, Plot_k, Plot_l, ncol = 2)
ggsave(b_value_rev_plots, file = b_value_rev_plots.png, width = 9, height = 9 )



