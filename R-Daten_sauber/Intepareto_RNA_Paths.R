
P1 <- snakemake@input[[1]]
P2 <- snakemake@input[[2]]
H3 <- snakemake@input[[3]]
H4 <- snakemake@input[[4]]
G5 <- snakemake@input[[5]]
G6 <- snakemake@input[[6]]

Kallisto_path_file <- snakemake@output[[1]]

Kallisto_path <- as.data.frame(c("PC","PC","HC","HC","GC","GC"))
colnames(Kallisto_path) <- c("condition")

Kallisto_path$files <- c(P1, P2, H3, H4, G5, G6)


write.table(Kallisto_path, file = Kallisto_path_file, row.names = FALSE, sep=";", quote = FALSE)