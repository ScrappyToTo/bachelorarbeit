P1_i <- snakemake@input[[1]]
P2_i <- snakemake@input[[2]]
H3_i <- snakemake@input[[3]]
H4_i <- snakemake@input[[4]]
G5_i <- snakemake@input[[5]]
G6_i <- snakemake@input[[6]]

P1_o <- snakemake@output[[1]]
P2_o <- snakemake@output[[2]]
H3_o <- snakemake@output[[3]]
H4_o <- snakemake@output[[4]]
G5_o <- snakemake@output[[5]]
G6_o <- snakemake@output[[6]]

library(readr)

P1 <- read_delim(P1_i,"\t", escape_double = FALSE, trim_ws = TRUE) 
P1 <- separate(data = P1, col = "target_id", into = c("target_id", "right"), sep = "\\.")
P1$right <- NULL
row.names(P1) <- c()
write.table(P1, file=P1_o, quote=FALSE, sep='\t', row.names = FALSE)

P2 <- read_delim(P2_i,"\t", escape_double = FALSE, trim_ws = TRUE) 
P2 <- separate(data = P2, col = "target_id", into = c("target_id", "right"), sep = "\\.")
P2$right <- NULL
row.names(P2) <- c()
write.table(P2, file=P2_o, quote=FALSE, sep='\t', row.names = FALSE)

H3 <- read_delim(H3_i,"\t", escape_double = FALSE, trim_ws = TRUE) 
H3 <- separate(data = H3, col = "target_id", into = c("target_id", "right"), sep = "\\.")
H3$right <- NULL
row.names(H3) <- c()
write.table(H3, file= H3_o, quote=FALSE, sep='\t', row.names = FALSE)

H4 <- read_delim(H4_i,"\t", escape_double = FALSE, trim_ws = TRUE) 
H4 <- separate(data = H4, col = "target_id", into = c("target_id", "right"), sep = "\\.")
H4$right <- NULL
row.names(H4) <- c()
write.table(H4, file=H4_o, quote=FALSE, sep='\t', row.names = FALSE)

G5 <- read_delim(G5_i,"\t", escape_double = FALSE, trim_ws = TRUE) 
G5 <- separate(data = G5, col = "target_id", into = c("target_id", "right"), sep = "\\.")
G5$right <- NULL
row.names(G5) <- c()
write.table(G5, file=G5_o, quote=FALSE, sep='\t', row.names = FALSE)

G6 <- read_delim(G6_i,"\t", escape_double = FALSE, trim_ws = TRUE) 
G6 <- separate(data = G6, col = "target_id", into = c("target_id", "right"), sep = "\\.")
G6$right <- NULL
row.names(G6) <- c()
write.table(G6, file=G6_o, quote=FALSE, sep='\t', row.names = FALSE)