---
title: "R Notebook"
output: html_notebook
---

```{r}
#library
library(ggplot2)
library(gridExtra)
library(rPref)
options(scipen = 999) 
```


```{r}
PGsleuth <- read.csv("Z:/Tobias/R_Git_Sonstiges/Pipeline/Outputs/V90/diff_exp/PG_new_sleuth_results_WT_gene_log2FC.csv")
HGsleuth <- read.csv("Z:/Tobias/R_Git_Sonstiges/Pipeline/Outputs/V90/diff_exp/HG_new_sleuth_results_WT_gene_log2FC.csv")
PHsleuth <- read.csv("Z:/Tobias/R_Git_Sonstiges/Pipeline/Outputs/V90/diff_exp/PH_new_sleuth_results_WT_log2FC.csv")
tpms <- read.csv( "Z:/Tobias/R_Git_Sonstiges/Pipeline/Outputs/V90/ZS/tpms_gene_1tpm_PHG.csv")
tpms_no_filter <- read.csv("Z:/Tobias/R_Git_Sonstiges/Pipeline/Outputs/V90/ZS/tpms_gene_PHG.csv")
```

```{r}

#volcano plots for differential RNA expression between PC, GC, HC calculated with Sleuth
  #function for PG
PGPlf <- function(x,y){
  PGPlfd <- PGsleuth[,c("ext_gene", x,y)]
  colnames(PGPlfd)<- c("ext_gene", "x","y")
  results <- ggplot(PGPlfd,aes(x,-log10(y)))+geom_point()+xlab(x)+ylab(y) + ggtitle("PG")+ 
      geom_text(data = PGsleuth, aes(x = b, y = -log10(qval),label=ifelse((-log10(qval) > 30 | b > 2.5),as.character(ext_gene),'')))
  return(results)
}


#function for HG


HGPlf <- function(x,y){
  HGPlfd <- HGsleuth[,c(x,y)]
  colnames(HGPlfd)<- c("x","y")
  results <- ggplot(HGPlfd,aes(x,-log10(y)))+geom_point()+xlab(x)+ylab(y) + ggtitle("HG")+
    geom_text(data = PGsleuth, aes(x = b, y = -log10(qval),label=ifelse((-log10(qval) > 30 | b > 2.5),as.character(ext_gene),'')))
  return(results)
}

  #function for PH
PHPlf <- function(x,y){
  PHPlfd <- PHsleuth[,c(x,y)]
  colnames(PHPlfd)<- c("x","y")
  results <- ggplot(PHPlfd,aes(x,-log10(y)))+geom_point()+xlab(x)+ylab(y)+ ggtitle("PH")+
    geom_text(data = PGsleuth, aes(x = b, y = -log10(qval),label=ifelse((-log10(qval) > 30),as.character(ext_gene),'')),hjust=0,vjust=0)+
    geom_text(data = PGsleuth, aes(x = b, y = -log10(qval),label=ifelse((b > 2.5),as.character(ext_gene),'')),hjust=0,vjust=0)
  return(results)
}

  #calculation for different volcano plots
Pl_PG_bq <- PGPlf("b","qval") 
Pl_PG_bs <- PGPlf("b","se_b")
Pl_PG_qs <- PGPlf("qval","se_b")
Pl_HG_bq <- HGPlf("b","qval")
Pl_HG_bs <- HGPlf("b","se_b")
Pl_HG_qs <- HGPlf("qval","se_b")
Pl_PH_bq <- PHPlf("b","qval")
Pl_PH_bs <- PHPlf("b","se_b")
Pl_PH_qs <- PHPlf("qval","se_b")


  #volcano plot arrange
volcanoplots <- grid.arrange(Pl_HG_bq,Pl_PG_bq,Pl_PH_bq)

#ggsave(volcanoplots, file = "Z:/Tobias/Volcanoplot_test.png")
```

```{r}
#library(ggrepel)

ggplot(PGsleuth,aes(b,-log10(qval)))+geom_point(colour = "skyblue3")+ ggtitle("PG")+  theme_bw() +
  geom_text(data = PGsleuth, aes(x = b, y = -log10(qval),label=ifelse((-log10(qval) > 25| b > 4 |b < -5),as.character(ext_gene),'')),hjust=0,vjust=0,nudge_x =0.2, check_overlap = TRUE) +   geom_text(data = PGsleuth, aes(x = b, y = -log10(qval),label=ifelse((ext_gene == "Chil1"| ext_gene == "Ndufa4l2"| ext_gene == "Prg4"| ext_gene == "Eps8l2" | ext_gene == "Foxo1" | ext_gene == "Foxn3" ),as.character(ext_gene),'')),hjust=0,vjust=0,nudge_x =0.2, colour = "red3")             

ggplot(PHsleuth,aes(b,-log10(qval)))+geom_point()+ ggtitle("PH")+ 
     geom_text(data = PHsleuth, aes(x = b, y = -log10(qval),label=ifelse((-log10(qval) > 25| b > 4 |b < -5 | ext_gene == "Chil1"| ext_gene == "Ndufa4l2"),as.character(ext_gene),'')),hjust=0,vjust=0, nudge_x = 0.2)

ggplot(HGsleuth,aes(b,-log10(qval)))+geom_point()+ ggtitle("HG")+ 
     geom_text(data = HGsleuth, aes(x = b, y = -log10(qval),label=ifelse((-log10(qval) > 25| b > 4 |b < -5 | ext_gene == "Chil1"| ext_gene == "Ndufa4l2"),as.character(ext_gene),'')),hjust=0,vjust=0, nudge_x = 0.2)

```
```{r}
Vulcanoplot_function <- function(x, y){ggplot(x,aes(b,-log10(qval)))+geom_point(colour = "skyblue3")+ ggtitle(y)+  theme_bw() +
  geom_text(data = x, aes(x = b, y = -log10(qval),label=ifelse((-log10(qval) > 25| b > 2 |b < -5),as.character(ext_gene),'')),hjust=0,vjust=0,nudge_x =0.2, check_overlap = TRUE) +   geom_text(data = x, aes(x = b, y = -log10(qval),label=ifelse((ext_gene == "Chil1"| ext_gene == "Ndufa4l2"| ext_gene == "Prg4"| ext_gene == "Eps8l2" | ext_gene == "Foxo1" | ext_gene == "Foxn3" ),as.character(ext_gene),'')),hjust=0,vjust=0,nudge_x =0.2, colour = "red3")
}

Vulcane_PG <- Vulcanoplot_function(PGsleuth, "PG")
Vulcane_PH <- Vulcanoplot_function(PHsleuth, "PH")
Vulcane_HG <- Vulcanoplot_function(HGsleuth, "HG")


volcanoplots <- grid.arrange(Vulcane_PG, Vulcane_PH, Vulcane_HG)

ggsave(volcanoplots, file = "Z:/Tobias/Volcanoplot_test.png", width = 8, height = 10)
```

